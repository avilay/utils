import logging
from avilabsutils.decorators import debug

import random
from avilabsutils.decorators import retry, MaxRetriesExceededError

from avilabsutils.decorators import validate_func_args, InvalidArgValueError, InvalidArgTypeError
from avilabsutils.decorators import validate_method_args, InvalidArgValueError, InvalidArgTypeError


logging.basicConfig(level=logging.DEBUG)


@debug
def funky_add(a, b):
    if a > 100 and b > 100:
        return a + b
    elif 50 < a < 100 and 50 < b < 100:
        return a * b
    else:
        return a - b


def test_debug():
    funky_add(10, 20)
    funky_add(60, 70)
    funky_add(200, 300)


@retry(max_retries=2)
def unpredictable():
    if random.choice([True, False]):
        return 42
    else:
        raise RuntimeError('KA-BOOM!!')


def test_retry():
    for i in range(5):
        try:
            print(unpredictable())
        except MaxRetriesExceededError as e:
            print(e)


@validate_func_args([
    ('tn', float, lambda x: x < 3.141),
    ('ary', list, lambda x: len(x) > 1),
    ('lang', str, None)
])
def foo(tn, ary, lang='Python'):
    print(tn, ary, lang)


def test_func_validator():
    foo(2.718, [1, 2], lang='C')

    try:
        foo(6.626, [1, 2])
    except InvalidArgValueError as e:
        print(e)

    try:
        foo('pi', [1, 2])
    except InvalidArgTypeError as e:
        print(e)

    try:
        foo(2.718, [1, 2], lang=42)
    except InvalidArgTypeError as e:
        print(e)


class Bar:
    @validate_method_args([
        ('tn', float, lambda x: x < 3.141),
        ('ary', list, lambda x: len(x) > 1),
        ('lang', str, None)
    ])
    def foo(self, tn, ary, lang):
        print(tn, ary, lang)


def test_method_validator():
    bar = Bar()

    bar.foo(2.718, [1, 2], lang='C')

    try:
        bar.foo(6.626, [1, 2])
    except InvalidArgValueError as e:
        print(e)

    try:
        bar.foo('pi', [1, 2])
    except InvalidArgTypeError as e:
        print(e)

    try:
        bar.foo(2.718, [1, 2], lang=42)
    except InvalidArgTypeError as e:
        print(e)


if __name__ == '__main__':
    test_debug()
    test_retry()
    test_func_validator()
    test_method_validator()






