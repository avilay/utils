from avilabsutils import print_success, print_code, print_warn, print_error, print_now

print_success('This will print in green')
print_code('This will print in cyan')
print_warn('This will be in yellow')
print_error('And this will be in red')

print_now('This will be printed immediately')
