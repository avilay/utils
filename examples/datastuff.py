import csv
import os.path as path
import numpy as np
from avilabsutils.data import DataSession


def create(tbd=True):
    """
    Create a new db with a new table and a new schema
    """
    dsess = DataSession.new('bakery')
    cookies = dsess.table('cookies')
    cookies.add_column('flavor', 'text')
    cookies.add_column('calories', 'int')

    cookies.append_rows([
        {'flavor': 'Chocolate Chip', 'calories': 200},
        {'flavor': 'Snicker Doodle', 'calories': 220},
        {'flavor': 'Oatmeal Raisin', 'calories': 180}
    ])

    # This column is added to an existing table
    cookies.add_column('servings', 'int')

    if tbd:
        DataSession.delete('bakery')


def read_1():
    create(tbd=False)

    dsess = DataSession.load('bakery')
    high_cal_cookies = dsess.query('select * from cookies where calories > ?', [190])

    # This will print a nicely formatted html table in notebook
    print(high_cal_cookies)

    # Now delete the db
    DataSession.delete('bakery')


def read_2():
    create(tbd=False)

    dsess = DataSession.load('bakery')
    cals = dsess.query('select calories, servings from cookies').values(dtype=np.float32)
    print(cals)

    DataSession.delete('bakery')


def update():
    create(tbd=False)

    dsess = DataSession.load('bakery')

    # Do a selective update - similar to update from where
    # First get a mutable copy of the query results - by default the results are immutable
    high_cal_cookies = dsess.query('select * from cookies where calories > ?', [190]).mutable_copy()
    for high_cal_cookie in high_cal_cookies:
        high_cal_cookie['servings'] = high_cal_cookie['calories'] // 2
    dsess.table('cookies').update_rows(high_cal_cookies, ['servings'])

    # Now delete this db
    DataSession.delete('bakery')


def delete():
    create(tbd=False)

    dsess = DataSession.load('bakery')
    print(dsess.query('select * from cookies'))
    cookies = dsess.table('cookies')
    cookies.delete_rows([1, 3])
    # Can also say -
    # cookies.delete_rows([{'_index': 1, '_index': 3}])
    print(dsess.query('select * from cookies'))

    # Now delete this db
    DataSession.delete('bakery')


def load_file1():
    """
    I have a csv file that I want to turn into a new table in a new db
    """
    dsess = DataSession.new('bakery')
    cookies = dsess.table('cookies')
    filepath = path.join(path.dirname(__file__), 'cookies_1.csv')
    with open(filepath, 'rt') as f:
        reader = csv.reader(f)
        # Setting infer_schema tells cookies to add columns if they don't already exist
        # which in this case are all the columns
        cookies.append_file(reader, infer_schema=True)

    print(dsess.query('select * from cookies'))

    DataSession.delete('bakery')


def load_file2():
    """
    I have a csv file that I want to append to an existing table.
    """
    create(tbd=False)
    dsess = DataSession.load('bakery')
    print('\nBefore')
    print(dsess.query('select * from cookies'))

    cookies = dsess.table('cookies')
    filepath = path.join(path.dirname(__file__), 'cookies_2.csv')
    with open(filepath, 'rt') as f:
        reader = csv.reader(f)
        cookies.append_file(reader)

    print('\nAfter')
    print(dsess.query('select * from cookies'))

    DataSession.delete('bakery')


def load_file3():
    """
    I want to add a few columns from a csv file to an existing table. Some of the columns that
    I want to add are already present in the table, some are not.
    """
    create(tbd=False)

    dsess = DataSession.load('bakery')
    print(dsess.query('select * from cookies'))

    cookies = dsess.table('cookies')
    # At this point cookies has 3 columns - flavor, calories, servings

    # Load cookies.csv which has 5 columns - flavor, calories, servings, sugar, and price
    # I just want to load flavor, calories, servings, and sugar into my table
    # And because sugar column does not exist, I want DataSession to automatically create it
    filepath = path.join(path.dirname(__file__), 'cookies_3.csv')
    with open(filepath, 'rt') as f:
        reader = csv.reader(f)
        columns = ['flavor', 'calories', 'servings', 'sugar']
        cookies.append_file(reader, columns=columns, infer_schema=True)

    print(dsess.query('select * from cookies'))

    DataSession.delete('bakery')


load_file3()
