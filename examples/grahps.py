# flake8: noqa
import numpy as np
from avilabsutils.plotting import *


def simple():
    angles = np.linspace(-np.pi, np.pi, 256, endpoint=True)

    cosline = Line(x=angles, y=np.cos(angles))
    sinline = Line(x=angles, y=np.sin(angles))

    trigchart = Chart()
    trigchart.plots = [cosline, sinline]

    frame = Frame()
    frame.layout(nrows=1, ncols=1)
    frame.charts = [trigchart]

    frame.show()


def multi():
    angles = np.linspace(-np.pi, np.pi, 256, endpoint=True)

    cosline = Line(x=angles, y=np.cos(angles))
    sinline = Line(x=angles, y=np.sin(angles))

    trigchart = Chart()
    trigchart.plots = [cosline, sinline]

    epochs = list(range(1, 6))

    loss_hpA = [0.98, 0.87, 0.71, 0.5, 0.63]
    loss_hpA_line = Line(x=epochs, y=loss_hpA)

    loss_hpB = [0.99, 0.72, 0.64, 0.53, 0.44]
    loss_hpB_line = Line(x=epochs, y=loss_hpB)

    ml_chart = Chart()
    ml_chart.plots = [loss_hpA_line, loss_hpB_line]

    frame = Frame()
    frame.layout(nrows=1, ncols=2)
    frame.charts = [trigchart, ml_chart]

    frame.show()


def fancy():
    angles = np.linspace(-np.pi, np.pi, 256, endpoint=True)

    cosline = Line(x=angles, y=np.cos(angles), label='cos(x)', hex_color='#ee28f1', linewidth=0.5, linestyle=LineStyle.DASHED)
    sinline = Line(x=angles, y=np.sin(angles))
    sinline.label = 'sin(x)'
    sinline.hex_color = '#8328f1'
    sinline.linewidth = 0.5
    sinline.linestyle = LineStyle.DOTTED

    trigchart = Chart(legend_location=LegendLocation.UPPER_LEFT, title='Trig', x_label='Radians', grid=True)
    trigchart.plots = [cosline, sinline]

    epochs = list(range(1, 6))

    loss_hpA = [0.98, 0.87, 0.71, 0.5, 0.63]
    loss_hpA_line = Line(x=epochs, y=loss_hpA, label='Hyperparams A', marker=Marker.CIRCLE)

    loss_hpB = [0.99, 0.72, 0.64, 0.53, 0.44]
    loss_hpB_line = Line(x=epochs, y=loss_hpB, label='Hyperparams B', marker=Marker.POINT)

    ml_chart = Chart(legend_location=LegendLocation.UPPER_RIGHT)
    ml_chart.plots = [loss_hpA_line, loss_hpB_line]
    ml_chart.x_limits = (1, 10)
    ml_chart.y_limits = (0, 1.5)
    ml_chart.x_label = 'Epochs'
    ml_chart.y_label = 'Loss'
    ml_chart.title = 'CNN Training'

    frame = Frame(width=10, height=6)
    frame.layout(nrows=1, ncols=2)
    frame.charts = [trigchart, ml_chart]

    frame.show()



def main_old():
    angles = np.linspace(-np.pi, np.pi, 256, endpoint=True)

    cosline = Line(x=angles, y=np.cos(angles))
    cosline.label = 'cos(x)'
    cosline.linestyle = LineStyle.DASHED

    sinline = Line(x=angles, y=np.sin(angles), label='sin(x)', linestyle=LineStyle.DASHDOT)

    trigchart = Chart(legend_location=LegendLocation.UPPER_LEFT, x_label='Radians', y_label='value')
    trigchart.plots = [cosline, sinline]
    trigchart.title = 'Trig Chart'
    trigchart.grid = True

    # dow = list(range(1, 6))

    # amzn = [1702.00, 1650.59, 1708.75, 1660.61, 1717.12]
    # amznline = Line(x=dow, y=amzn, label='AMZN', marker=Marker.STAR, linewidth=0.2)
    # amznline.hex_color = '#f28d46'

    # goog = [1143.08, 1128.00, 1118.46, 1122.06, 1114.22]
    # googline = Line(x=dow, y=goog, label='GOOG', hex_color='#298af2')
    # googline.marker = Marker.CIRCLE
    # googline.linewidth = 1.0

    # stockchart = Chart(legend_location=LegendLocation.UPPER_RIGHT, title='Stocks')
    # stockchart.plots = [amznline, googline]
    # stockchart.x_limits = (-3, 7)
    # stockchart.y_limits = (1000, 2000)

    frame = Frame(size=(10, 6))
    frame.charts = [trigchart, stockchart]
    frame.layout(nrows=1, ncols=2)
    frame.show()


if __name__ == '__main__':
    simple()
    # multi()
    # fancy()
