from datetime import datetime, timezone
import json
from avilabsutils import JsonConverter

obj = {
    'user_id': 1,
    'created_on': datetime(2016, 1, 1, tzinfo=timezone.utc),
    'name': 'Cookie Monster'
}
print(json.dumps(obj, cls=JsonConverter))
