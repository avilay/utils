import numpy as np
from tabulate import tabulate
from IPython.display import HTML, display


def isnotebook():
    try:
        shell = get_ipython().__class__.__name__
        if shell == 'ZMQInteractiveShell':
            return True   # Jupyter notebook or qtconsole
        elif shell == 'TerminalInteractiveShell':
            return False  # Terminal running IPython
        else:
            return False  # Other type (?)
    except NameError:
        return False      # Probably standard Python interpreter


class DataView:
    """
    This is an immutable class that holds the results of any query.
    It is possible to iterate through the results of the query or print the
    entire data view as a pretty HTML table in a notebook.
    """
    def __init__(self, cursor, rows):
        self._cols = [col[0] for col in cursor.description]
        self._rows = rows

    def _drow(self, row):
        drow = {}
        for i, col in enumerate(self._cols):
            drow[col] = row[i]
        return drow

    def __iter__(self):
        for row in self._rows:
            yield self._drow(row)

    def __getitem__(self, key):
        if isinstance(key, str):
            # key is colname
            colndx = self._cols.index(key)
            col = [None] * len(self._rows)
            for rowndx, row in enumerate(self._rows):
                col[rowndx] = row[colndx]
            return col
        elif isinstance(key, int):
            # key is rowndx
            return self._drow(self._rows[key])

    def __repr__(self):
        if isnotebook():
            html_tbl = tabulate(self._rows, headers=self._cols, tablefmt='html')
            display(HTML(html_tbl))
            return ''
        else:
            txt_tbl = tabulate(self._rows, headers=self._cols)
            return txt_tbl

    def mutable_copy(self):
        all_rows = []
        for row in self._rows:
            all_rows.append(self._drow(row))
        return all_rows

    def values(self, dtype):
        """
        Convert the data view into a 2D numpy array. This will only work if all the columns
        in the data view are numeric (int or float).
        """
        num_rows = len(self._rows)
        num_cols = len(self._cols)
        vals = np.zeros((num_rows, num_cols), dtype=dtype)
        for i, row in enumerate(self._rows):
            for j, cell in enumerate(row):
                vals[i, j] = cell

        return vals

    def __len__(self):
        return len(self._rows)
