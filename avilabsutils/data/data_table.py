from collections import namedtuple

Column = namedtuple('Column', [
    'cid', 'name', 'type', 'notnull', 'dflt_value', 'pk'])


class DataTable:
    """Represents a table in SQLite.

    Provides methods to mutate the table like adding columns, adding and
    updating rows.
    """
    def _refresh_schema(self, cur):
        cur.execute(f'PRAGMA table_info([{self._name}])')
        for row in cur:
            name = row[1]
            self._cols[name] = Column(
                cid=row[0],
                name=row[1],
                type=row[2],
                notnull=row[3],
                dflt_value=row[4],
                pk=row[5]
            )

    def _infer_dtype(self, fld):
        if isinstance(fld, float):
            return 'float'
        elif isinstance(fld, int):
            return 'int'
        elif isinstance(fld, str):
            return 'text'
        else:
            return 'blob'

    def __init__(self, conn, name):
        """Loads the table if it exists

        If a table with the given name exists in the db, the constructor will
        load its schema. Otherwise it will simply instantiate an empty table
        structure.

        :param conn: A live SQL connection
        :param name: Name of the table
        """
        self._name = name
        self._conn = conn
        self._cols = {}
        cur = conn.cursor()
        self._refresh_schema(cur)
        cur.close()

    def __repr__(self):
        tbl = self._name + ':'
        for col in self._cols:
            tbl += f'\n\t{col}'
        return tbl

    def create(self):
        """Creates an almost empty table in the db

        Creates a table with a single column called _index. This column is
        supposed to be a proxy for the index into the dataset if the entire
        dataset were to be loaded in a list.
        """
        cur = self._conn.cursor()
        cur.execute(f'CREATE TABLE {self._name} (_index INTEGER PRIMARY KEY)')
        self._refresh_schema(cur)
        cur.close()

    def add_column(self, name, dtype):
        """Adds a new column to the table

        Regardless of whether this is an old table or a newly created one,
        this method adds a new empty column to the table.
        :param name: Name of the column
        :param dtype: Data type of the column, any SQLite type is allowed
        """
        cur = self._conn.cursor()
        cur.execute(f'ALTER TABLE {self._name} add {name} {dtype}')
        self._refresh_schema(cur)
        cur.close()

    def append_rows(self, rows):
        """Adds new rows to the table

        :param rows: A list of dict like object with the column names in the
        table as keys. All columns in the table must be present in the rows. Any additional columns
        including the _index column are ignored.
        It assumes that the data types in the rows are legit.
        """
        colnames = list(self._cols.keys())
        colnames.remove('_index')
        all_colnames = ','.join(colnames)
        val_placeholders = ','.join(['?'] * len(colnames))
        sql = f'''
        INSERT INTO {self._name} ({all_colnames})
        VALUES ({val_placeholders})
        '''
        ins_rows = []
        for row in rows:
            ins_row = []
            for colname in colnames:
                ins_row.append(row[colname])
            ins_rows.append(ins_row)
        cur = self._conn.cursor()
        cur.executemany(sql, ins_rows)
        cur.close()

    def update_rows(self, rows, cols_to_update):
        """Update the specified columns in the given rows

        Updates the rows identified by their _index column (key).
        :param rows: A list of dict-like objects. It may have any number of
        keys but only the keys specified in the cols_to_update list are
        considered. All other keys are ignored.
        :param cols_to_update: Columns that are to be updated. Only these keys
        are considered in the given rows. It is expected that these keys will
        be present.
        """
        for col_to_update in cols_to_update:
            if col_to_update not in self._cols:
                raise RuntimeError(f'Unknown column {col_to_update}!')

        sql_frags = []
        for col_to_update in cols_to_update:
            sql_frags.append(f'{col_to_update} = ?')
        sql_frag = ','.join(sql_frags)
        sql = f'UPDATE {self._name} SET {sql_frag} WHERE _index = ?'

        up_rows = []
        for row in rows:
            up_row = []
            for col_to_update in cols_to_update:
                up_row.append(row[col_to_update])
            up_row.append(row['_index'])
            up_rows.append(up_row)

        cur = self._conn.cursor()
        cur.executemany(sql, up_rows)
        cur.close()

    def delete_rows(self, rows):
        """Delete the specified rows

        Rows to be deleted are given in the rows param.
        :param rows: Can be a list of integers, in which case they will be assumed
        to mean the _index values of the rows to be deleted.
        Can also be a list of dict-like objects with an _index key in each dict.
        """
        ndxs = []
        for row in rows:
            if isinstance(row, int):
                ndxs.append(row)
            elif isinstance(row, dict) and '_index' in row:
                ndxs.append(int(row['_index']))
        all_ndxs = ','.join([str(ndx) for ndx in ndxs])
        sql = f'DELETE FROM {self._name} WHERE _index IN ({all_ndxs})'

        cur = self._conn.cursor()
        cur.execute(sql)
        cur.close()

    def append_file(self, csvreader, columns='__all__', infer_schema=False):
        header = next(csvreader)
        rows = []

        # Check if specified columns are actually present in the data file
        all_colnames = [colname.strip().lower() for colname in header]
        if columns == '__all__':
            columns = all_colnames
        else:
            columns = [col.strip().lower() for col in columns]
            for column in columns:
                if column not in all_colnames:
                    raise RuntimeError(f'{column} is not present in the data file!')

        # Get the index number of columns that are to be added
        ndxs = []
        for i, colname in enumerate(all_colnames):
            if colname in columns:
                ndxs.append(i)

        # Check if the columns to be added are present in the table
        # Create them if infer_schema is set
        line = next(csvreader)  # Get the first line to infer schema
        row = {}
        for ndx in ndxs:
            colname = all_colnames[ndx]
            fld = line[ndx]
            if colname not in self._cols:
                if infer_schema:
                    dtype = self._infer_dtype(fld)
                    self.add_column(colname, dtype)
                else:
                    raise RuntimeError(f'Column {colname} is not present in the table!')
            row[colname] = fld
        rows.append(row)

        # Add the remaining rows
        for line in csvreader:
            row = {}
            for ndx in ndxs:
                colname = all_colnames[ndx]
                fld = line[ndx]
                row[colname] = fld
            rows.append(row)

        self.append_rows(rows)
