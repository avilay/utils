from enum import Enum, auto
from typing import List
from .base import NumericPlot


class LineStyle(Enum):
    SOLID = auto()
    DASHED = auto()
    DASHDOT = auto()
    DOTTED = auto()


class Marker(Enum):
    POINT = auto()
    PIXEL = auto()
    CIRCLE = auto()
    TRIANGLE_DOWN = auto()
    TRIANGLE_UP = auto()
    TRIANGLE_LEFT = auto()
    TRIANGLE_RIGHT = auto()
    SQAURE = auto()
    PENTAGON = auto()
    STAR = auto()
    HEXAGON = auto()
    PLUS = auto()
    X = auto()
    DIAMOND = auto()
    THIN_DIAMOND = auto()
    VLINE = auto()
    HLINE = auto()


class Line(NumericPlot):
    linestyle_map = {
        LineStyle.SOLID: '-',
        LineStyle.DASHED: '--',
        LineStyle.DASHDOT: '-.',
        LineStyle.DOTTED: ':'
    }

    marker_map = {
        Marker.POINT: '.',
        Marker.PIXEL: ',',
        Marker.CIRCLE: 'o',
        Marker.TRIANGLE_DOWN: 'v',
        Marker.TRIANGLE_UP: '^',
        Marker.TRIANGLE_LEFT: '<',
        Marker.TRIANGLE_RIGHT: '>',
        Marker.SQAURE: 's',
        Marker.PENTAGON: 'p',
        Marker.STAR: '*',
        Marker.HEXAGON: 'h',
        Marker.PLUS: '+',
        Marker.X: 'x',
        Marker.DIAMOND: 'D',
        Marker.THIN_DIAMOND: 'd',
        Marker.VLINE: '|',
        Marker.HLINE: '_'
    }

    def __init__(
        self,
        *,
        x: List[float],
        y: List[float],
        label: str=None,
        hex_color: str=None,
        linewidth: float=None,
        linestyle: LineStyle=None,
        marker: Marker=None
    ):
        super().__init__(x, y)
        self.label = label
        self.hex_color = hex_color
        self.linewidth = linewidth
        self.linestyle = linestyle
        self.marker = marker

    def draw(self, figure):
        kwargs = {}
        if self.label:
            kwargs['label'] = self.label
        if self.linewidth:
            kwargs['linewidth'] = self.linewidth
        if self.linestyle:
            kwargs['linestyle'] = Line.linestyle_map[self.linestyle]
        if self.marker:
            kwargs['marker'] = Line.marker_map[self.marker]
        if self.hex_color:
            kwargs['color'] = self.hex_color
        figure.plot(self._x, self._y, **kwargs)
