from enum import Enum, auto
from typing import List, Union, Callable, Tuple
from .base import NumericPlot


class LineStyle(Enum):
    SOLID = auto()
    DASHED = auto()
    DASHDOT = auto()
    DOTTED = auto()


class Graph(NumericPlot):
    linestyle_map = {
        LineStyle.SOLID: '-',
        LineStyle.DASHED: '--',
        LineStyle.DASHDOT: '-.',
        LineStyle.DOTTED: ':'
    }

    def __init__(
        self,
        *,
        x_range: Tuple[float, float],
        scalar_func: Callable[[float], float]=None,
        vector_func: Callable[[List[float]], float]=None,
        label: str=None,
        hex_color: str=None,
        linewidth: float=None,
        linestyle: LineStyle=None
    ):
        
        super().__init__(x, y)
        self.label = label
        self.hex_color = hex_color
        self.linewidth = linewidth
        self.linestyle = linestyle

    def draw(self, figure):
        kwargs = {}
        if self.label:
            kwargs['label'] = self.label
        if self.linewidth:
            kwargs['linewidth'] = self.linewidth
        if self.linestyle:
            kwargs['linestyle'] = Graph.linestyle_map[self.linestyle]
        if self.hex_color:
            kwargs['color'] = self.hex_color
        figure.plot(self._x, self._y, **kwargs)
