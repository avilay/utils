from datetime import datetime
from enum import Enum
from typing import Tuple, List  # NOQA
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt


class Frame:
    def __init__(self, width: float=None, height: float=None):
        self.size = (width, height) if width and height else None
        self._nrows = None
        self._ncols = None
        self.charts = []

    def layout(self, nrows, ncols):
        self._nrows = nrows
        self._ncols = ncols

    def show(self):
        if self._nrows * self._ncols != len(self.charts):
            raise ValueError('Number of rows and columns do not add upto the number of charts!')

        if self.size:
            plt.figure(figsize=self.size)
        for idx, chart in enumerate(self.charts, start=1):
            fig = plt.subplot(self._nrows, self._ncols, idx)
            for plot in chart.plots:
                plot.draw(fig)
            chart.configure(fig)
        plt.show()


class LegendLocation(Enum):
    BEST = 0
    UPPER_RIGHT = 1
    UPPER_LEFT = 2
    LOWER_LEFT = 3
    LOWER_RIGHT = 4
    RIGHT = 5
    CENTER_LEFT = 6
    CENTER_RIGHT = 7
    LOWER_CENTER = 8
    UPPER_CENTER = 9
    CENTER = 10


class Chart:
    def __init__(
        self,
        *,
        legend_location: LegendLocation=None,
        title: str=None,
        x_limits: Tuple[float, float]=None,
        x_ticks: List[int]=None,
        x_label: str=None,
        y_limits: Tuple[float, float]=None,
        y_ticks: List[int]=None,
        y_label: str=None,
        grid: bool=None,
        origin: Tuple[float, float]=None
    ):
        self.legend_location = legend_location
        self.title = title

        self.x_limits = x_limits
        self.x_ticks = x_ticks
        self.x_label = x_label

        self.y_limits = y_limits
        self.y_ticks = y_ticks
        self.y_label = y_label

        self.grid = grid
        self.origin = origin

        self.plots = []

    def configure(self, figure):
        if self.legend_location:
            figure.legend(loc=self.legend_location.value)
        if self.title:
            figure.set_title(self.title)
        if self.x_limits:
            figure.set_xlim(*self.x_limits)
        # TODO: x_ticks
        if self.x_label:
            figure.set_xlabel(self.x_label)
        if self.y_limits:
            figure.set_ylim(*self.y_limits)
        # TODO: y_ticks
        if self.y_label:
            figure.set_ylabel(self.y_label)
        if self.grid:
            figure.grid(True)
        # TODO: origin


class Plot(ABC):
    @abstractmethod
    def draw(self, figure):
        pass


class NumericPlot(Plot):
    def __init__(self, x: List[float], y: List[float]):
        self._x = x
        self._y = y


class CategoryPlot(Plot):
    def __init__(self, x: List[str], y: List[float]):
        self._x = x
        self._y = y


class TimeseriesPlot(Plot):
    def __init__(self, x: List[datetime], y: List[float]):
        self._x = x
        self._y = y
